#include <iostream>
#include "manu.hpp"
#include "menus.hpp"

using namespace std;

int main() {

    Menu mn = Finish;
    int mn_1 = mn;

    do {
        mainMenu();
        cin>>mn_1;

        switch (mn_1) {              // Выбор режима
            case (Square_matrix):
                Square_matrix_menu();
                break;
            case (Rectangular_matrix):
                Rectangular_matrix_menu();
                break;
            case (Diagonal_matrix):
                Diagonal_matrix_menu();
                break;
            case (Triangle_matrix):
                Triangle_matrix_menu();
                break;
            case (Square_test):
                Square_matrix_test();
                break;
            case (Rectangular_test):
                Rectangular_matrix_test();
                break;
            case (Diagonal_test):
                Diagonal_matrix_test();
                break;
            case (Triangle_test):
                Triangle_matrix_test();
                break;
            case(Finish):
                cout<<"GOOD BAY";
                break;
            default:
                cout<<"Repid input\n";
                break;
        }
    } while(mn_1 != 9);

    return 0;
}