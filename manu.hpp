#ifndef LAB3_MANU_HPP
#define LAB3_MANU_HPP

//#include "ArraySquareMatrix.hpp"
//#include "ListSquareMatrix.hpp"
//#include "ArrayRectangularMatrix.hpp"
//#include "ListRectangularMatrix.hpp"
//#include "ArrayDiagonalMatrix.hpp"
//#include "ListDiagonalMatrix.hpp"
//#include "ArrayTriangleMatrix.hpp"
//#include "ListTriangleMatrix.hpp"
#include "menus.hpp"
#include <iostream>

using namespace std;

enum Menu {
    Square_matrix = 1,
    Rectangular_matrix,
    Diagonal_matrix,
    Triangle_matrix,
    Square_test,
    Rectangular_test,
    Diagonal_test,
    Triangle_test,
    Finish
};

void mainMenu(){
    printf("1) Square_matrix\n");
    printf("2) Rectangular_matrix\n");
    printf("3) Diagonal_matrix\n");
    printf("4) Triangle_matrix\n");
    printf("5) Square_test\n");
    printf("6) Rectangular_test\n");
    printf("7) Diagonal_test\n");
    printf("8) Triangle_test\n");
    printf("9) Exit\n");
}

void Square_matrix_test() {
    int ** arr;
    arr = new int*[4];
    for (int i = 0; i < 4; i++) {
        arr[i] = new int[4];
        for (int j = 0; j < 4; j++) {
            arr[i][j] = i + j;
        }
    }

    ArraySquareMatrix<int> * matrix_1 = new ArraySquareMatrix<int>(4, arr);
    ListSquareMatrix<int> * matrix_2 = new ListSquareMatrix<int>(4, arr);

    ListSquareMatrix<int> * matrix_3 = new ListSquareMatrix<int>(4, arr);

    matrix_3->AddMatrix(matrix_2);

    printf("Array matrix\n");
    printarraymatrix(matrix_1);
    printf("List matrix\n");
    printlistmatrix(matrix_2);
    printf("List matrix (sum)\n");
    printlistmatrix(matrix_3);

    for (int i = 0; i < 4; i++) {
        delete arr[i];
    }
    delete arr;

    int * arr_1;
    arr_1 = new int[5];
    for (int i = 0; i < 5; i++) {
        arr_1[i] = 3;
    }

    int * arr_2;
    arr_2 = new int[5];
    for (int i = 0; i < 5; i++) {
        arr_2[i] = 3;
    }

    try{matrix_1->AddRowAndColumn(arr_1, 6, arr_2, 4);}
    catch (...) {cout << "Error!" << std::endl;}

    matrix_1->AddRowAndColumn(arr_1, 4, arr_2, 4);


    printf("Array matrix(with adding)\n");
    printarraymatrix(matrix_1);
    printf("List matrix(start matrix)\n");
    printlistmatrix(matrix_2);

    delete arr_1;
    delete arr_2;
}

void Rectangular_matrix_test() {
    int ** arr;
    arr = new int*[4];
    for (int i = 0; i < 4; i++) {
        arr[i] = new int[4];
        for (int j = 0; j < 4; j++) {
            arr[i][j] = i + j;
        }
    }

    ArrayRectangularMatrix<int> * matrix_1 = new ArrayRectangularMatrix<int>(4, 4, arr);
    ListRectangularMatrix<int> * matrix_2 = new ListRectangularMatrix<int>(4, 4 , arr);

    ListRectangularMatrix<int> * matrix_3 = new ListRectangularMatrix<int>(4, 4, arr);

    matrix_3->AddMatrix(matrix_2);

    printf("Array matrix\n");
    printarraymatrix_r(matrix_1);
    printf("List matrix\n");
    printlistmatrix_r(matrix_2);
    printf("List matrix (sum)\n");
    printlistmatrix_r(matrix_3);

    for (int i = 0; i < 4; i++) {
        delete arr[i];
    }
    delete arr;

    int * arr_1;
    arr_1 = new int[5];
    for (int i = 0; i < 5; i++) {
        arr_1[i] = 0;
    }

    int * arr_2;
    arr_2 = new int[5];
    for (int i = 0; i < 5; i++) {
        arr_2[i] = 0;
    }

    try{matrix_1->AddRow(arr_1, 6);}
    catch (...) {cout << "Error!" << std::endl;}

    matrix_1->AddRow(arr_1, 4);
    matrix_1->AddColumn(arr_2, 4);

    printf("Matrix(with adding)\n");
    printarraymatrix_r(matrix_1);
    printf("Matrix(start)\n");
    printlistmatrix_r(matrix_2);

    delete arr_1;
    delete arr_2;
}

void Diagonal_matrix_test() {
    int *a;
    a = new int[5];
    for (int i = 0; i < 5; i++) {
        a[i] = i + 1;
    }

    ArrayDiagonalMatrix<int> * matrix_1 = new ArrayDiagonalMatrix<int>(5, a);
    ListDiagonalMatrix<int> * matrix_2 = new ListDiagonalMatrix<int>(5, a);
    ListDiagonalMatrix<int> * matrix_3 = new ListDiagonalMatrix<int>(5, a);

    delete a;

    matrix_3->AddMatrix(matrix_2);
    printf("Array matrix\n");
    printarraymatrix_d(matrix_1);
    printf("List matrix\n");
    printlistmatrix_d(matrix_2);
    printf("List matrix (sum)\n");
    printlistmatrix_d(matrix_3);

    try{matrix_2->InsertRowOrColumn(7, 4);}
    catch (...) {cout << "Error!" << std::endl;}

    matrix_1->InsertRowOrColumn(2, 4);
    matrix_2->InsertRowOrColumn(2, 4);

    printf("Array matrix (after insert)\n");
    printarraymatrix_d(matrix_1);
    printf("List matrix (after insert)\n");
    printlistmatrix_d(matrix_2);
}

void Triangle_matrix_test() {
    int **a;
    a = new int *[4];
    for (int i = 0; i < 4; i++) {
        a[i] = new int[4];
        for (int j = 0; j < 4; j++) {
            if (i <= j) {
                a[i][j] = i + 1;
            } else {
                a[i][j] = 0;
            }
        }
    }

    ArrayTriangleMatrix<int> * matrix_1 = new ArrayTriangleMatrix<int>(4, a);
    ListTriangleMatrix<int> * matrix_2 = new ListTriangleMatrix<int>(4, a);



    printf("Array matrix\n");
    printarraymatrix_t(matrix_1);
    printf("List matrix\n");
    printlistmatrix_t(matrix_2);

    int * arr_1;
    arr_1 = new int[6];
    for (int i = 0; i < 6; i++) {
        arr_1[i] = 1;
    }

    matrix_1->AddRow(arr_1);
    matrix_1->AddColumn(arr_1);
    printf("Array matrix(with add)\n");
    printarraymatrix_t(matrix_1);
    printf("List matrix(start)\n");
    printlistmatrix_t(matrix_2);

    cout<<"Try to get index out of range\n";

    try{matrix_1->Get(6, 6);}
    catch (...) {cout << "Error!" << std::endl;}

    ListTriangleMatrix<int> * matrix_3 = new ListTriangleMatrix<int>(4, a);
    matrix_3->AddMatrix(matrix_2);

    printf("List matrix\n");
    printlistmatrix_t(matrix_2);
    printf("List matrix\n");
    printlistmatrix_t(matrix_3);

    for (int i = 0; i < 4; i++) {
        delete a[i];
    }
    delete a;
}


#endif //LAB3_MANU_HPP