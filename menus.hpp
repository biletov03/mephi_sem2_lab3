#ifndef LAB3_MENUS_H
#define LAB3_MENUS_H

#include "ArraySquareMatrix.hpp"
#include "ListSquareMatrix.hpp"
#include "ArrayRectangularMatrix.hpp"
#include "ListRectangularMatrix.hpp"
#include "ArrayDiagonalMatrix.hpp"
#include "ListDiagonalMatrix.hpp"
#include "ArrayTriangleMatrix.hpp"
#include "ListTriangleMatrix.hpp"
#include <iostream>

void Smatrixmenu() {
    printf("1) MultRow\n");
    printf("2) MultColumn\n");
    printf("3) AddRowByRow\n");
    printf("4) AddColumnByColumn\n");
    printf("5) SwapRows\n");
    printf("6) SwapColumns\n");
    printf("7) MultScalar\n");
    printf("8) GetNorm\n");
    printf("9) Print\n");
    printf("10) Back\n");
}


enum Smatrix {
    MultRow = 1,
    MultColumn,
    AddRowByRow,
    AddColumnByColumn,
    SwapRows,
    SwapColumns,
    MultScalar,
    GetNorm,
    Print,
    Back
};

void Dmatrixmenu() {
    printf("1) GetDimension\n");
    printf("2) MultScalar\n");
    printf("3) GetNorm\n");
    printf("4) Print\n");
    printf("5) Back\n");
}


enum Dmatrix {
    GetDimension_d = 1,
    MultScalar_d,
    GetNorm_d,
    Print_d,
    Back_d
};

template<typename T>
void printlistmatrix(ListSquareMatrix<T> * matrix) {
    int size = matrix->GetDimension();
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            cout<<matrix->Get(i, j)<<" ";
        }
        cout<<endl;
    }
    cout<<endl;
}

template<typename T>
void printarraymatrix(ArraySquareMatrix<T> * matrix) {
    int size = matrix->GetDimension();
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            cout<<matrix->Get(i, j)<<" ";
        }
        cout<<endl;
    }
    cout<<endl;
}


void Square_matrix_menu() {
    Smatrix tipe_1 = Back;
    int tipe = tipe_1;
    int n, place_from, place_in;


        printf("Input size of matrix\n");
        cin >> n;
        cout << "Input element of matrix, size :" << n << endl;
        int **a;
        a = new int *[n];
        for (int i = 0; i < n; i++) {
            a[i] = new int[n];
            for (int j = 0; j < n; j++) {
                cin >> a[i][j];
            }
        }

        ArraySquareMatrix<int> * matrix_1 = new ArraySquareMatrix<int>(n, a);
        ListSquareMatrix<int> * matrix_2 = new ListSquareMatrix<int>(n, a);
        for (int i = 0; i < n; i++) {
            delete a[i];
        }
        delete a;

    do {
        Smatrixmenu();
        cin>>tipe;
        switch(tipe) {
            case(MultRow):
                printf("Input coefficient and place:\n");
                cin>>n>>place_in;
                matrix_1->MultRow(place_in, n);
                matrix_2->MultRow(place_in, n);
                break;
            case(MultColumn):
                printf("Input coefficient and place:\n");
                cin>>n>>place_in;
                matrix_1->MultColumn(place_in, n);
                matrix_2->MultColumn(place_in, n);
                break;
            case(AddRowByRow):
                printf("Input coefficient and place from, in:\n");
                cin>>n>>place_from>>place_in;
                matrix_1->AddRowByRow(place_in, place_from, n);
                matrix_2->AddRowByRow(place_in, place_from, n);
                break;
            case(AddColumnByColumn):
                printf("Input coefficient and place from, in:\n");
                cin>>n>>place_from>>place_in;
                matrix_1->AddColumnByColumn(place_in, place_from, n);
                matrix_2->AddColumnByColumn(place_in, place_from, n);
                break;
            case(SwapRows):
                printf("Input elements of swap(i, j):\n");
                cin>>place_from>>place_in;
                matrix_1->SwapRows(place_in, place_from);
                matrix_2->SwapRows(place_in, place_from);
                break;
            case(SwapColumns):
                printf("Input elements of swap(i, j):\n");
                cin>>place_from>>place_in;
                matrix_1->SwapColumns(place_in, place_from);
                matrix_2->SwapColumns(place_in, place_from);
                break;
            case(MultScalar):
                printf("Input coefficient:\n");
                cin>>n;
                matrix_1->MultScalar(n);
                matrix_2->MultScalar(n);
                break;
            case(GetNorm):
                cout<<"Array matrix norm:\n"<<matrix_1->GetNorm()<<"\nArray matrix norm:\n"<<matrix_2->GetNorm()<<endl;
                break;
            case(Print):
                printf("Array matrix\n");
                printarraymatrix(matrix_1);
                printf("List matrix\n");
                printlistmatrix(matrix_2);
                break;
            case(Back):
                break;
            default:
                printf("Repid input");
                break;
        }
    } while(tipe != 10);
}


template<typename T>
void printlistmatrix_r(ListRectangularMatrix<T> * matrix) {
    int row = matrix->GetNumberRows();
    int column = matrix->GetNumberColumns();
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < column; j++) {
            cout<<matrix->Get(i, j)<<" ";
        }
        cout<<endl;
    }
    cout<<endl;
}

template<typename T>
void printarraymatrix_r(ArrayRectangularMatrix<T> * matrix) {
    int row = matrix->GetNumberRows();
    int column = matrix->GetNumberColumns();
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < column; j++) {
            cout<<matrix->Get(i, j)<<" ";
        }
        cout<<endl;
    }
    cout<<endl;
}

void Rectangular_matrix_menu() {
    Smatrix tipe_1 = Back;
    int tipe = tipe_1;
    int n, place_from, place_in;


    printf("Input size of matrix (row and column)\n");
    cin >> n >> place_in;
    cout << "Input element of matrix, size :" << n <<" x "<<place_in<<endl;
    int **a;
    a = new int *[n];
    for (int i = 0; i < n; i++) {
        a[i] = new int[place_in];
        for (int j = 0; j < place_in; j++) {
            cin >> a[i][j];
        }
    }

    ArrayRectangularMatrix<int> * matrix_1 = new ArrayRectangularMatrix<int>(n,place_in, a);
    ListRectangularMatrix<int> * matrix_2 = new ListRectangularMatrix<int>(n,place_in, a);
    for (int i = 0; i < n; i++) {
        delete a[i];
    }
    delete a;

    do {
        Smatrixmenu();
        cin>>tipe;
        switch(tipe) {
            case(MultRow):
                printf("Input coefficient and place:\n");
                cin>>n>>place_in;
                matrix_1->MultRow(place_in, n);
                matrix_2->MultRow(place_in, n);
                break;
            case(MultColumn):
                printf("Input coefficient and place:\n");
                cin>>n>>place_in;
                matrix_1->MultColumn(place_in, n);
                matrix_2->MultColumn(place_in, n);
                break;
            case(AddRowByRow):
                printf("Input coefficient and place from, in:\n");
                cin>>n>>place_from>>place_in;
                matrix_1->AddRowByRow(place_in, place_from, n);
                matrix_2->AddRowByRow(place_in, place_from, n);
                break;
            case(AddColumnByColumn):
                printf("Input coefficient and place from, in:\n");
                cin>>n>>place_from>>place_in;
                matrix_1->AddColumnByColumn(place_in, place_from, n);
                matrix_2->AddColumnByColumn(place_in, place_from, n);
                break;
            case(SwapRows):
                printf("Input elements of swap(i, j):\n");
                cin>>place_from>>place_in;
                matrix_1->SwapRows(place_in, place_from);
                matrix_2->SwapRows(place_in, place_from);
                break;
            case(SwapColumns):
                printf("Input elements of swap(i, j):\n");
                cin>>place_from>>place_in;
                matrix_1->SwapColumns(place_in, place_from);
                matrix_2->SwapColumns(place_in, place_from);
                break;
            case(MultScalar):
                printf("Input coefficient:\n");
                cin>>n;
                matrix_1->MultScalar(n);
                matrix_2->MultScalar(n);
                break;
            case(GetNorm):
                cout<<"Array matrix norm:\n"<<matrix_1->GetNorm()<<"\nArray matrix norm:\n"<<matrix_2->GetNorm()<<endl;
                break;
            case(Print):
                printf("Array matrix\n");
                printarraymatrix_r(matrix_1);
                printf("List matrix\n");
                printlistmatrix_r(matrix_2);
                break;
            case(Back):
                break;
            default:
                printf("Repid input");
                break;
        }
    } while(tipe != 10);
}
template<typename T>

void printlistmatrix_d(ListDiagonalMatrix<T> * matrix) {
    int size = matrix->GetDimension();
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (i == j) {
                cout<<matrix->Get(i, i)<<" ";
            } else {
                cout<<"0 ";
            }
        }
        cout<<endl;
    }
    cout<<endl;
}

template<typename T>
void printarraymatrix_d(ArrayDiagonalMatrix<T> * matrix) {
    int size = matrix->GetDimension();
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (i == j) {
                cout<<matrix->Get(i, i)<<" ";
            } else {
                cout<<"0 ";
            }
        }
        cout<<endl;
    }
    cout<<endl;
}

void Diagonal_matrix_menu() {
    Dmatrix tipe_1 = Back_d;
    int tipe = tipe_1;
    int n, place_from, place_in;


    printf("Input size of matrix\n");
    cin >> n;
    cout << "Input element of matrix, size :" << n <<endl;
    int *a;
    a = new int[n];
    for (int i = 0; i < n; i++) {
            cin >> a[i];
    }

    ArrayDiagonalMatrix<int> * matrix_1 = new ArrayDiagonalMatrix<int>(n, a);
    ListDiagonalMatrix<int> * matrix_2 = new ListDiagonalMatrix<int>(n, a);

    delete a;

    do {

        Dmatrixmenu();
        cin>>tipe;
        switch(tipe) {
            case(GetDimension_d):
                cout<<"Array matrix dimension:\n"<<matrix_1->GetDimension()<<"\nList matrix dimension:\n"<<matrix_2->GetDimension()<<endl;
                break;
            case(MultScalar_d):
                printf("Input coefficient:\n");
                cin>>n;
                matrix_1->MultScalar(n);
                matrix_2->MultScalar(n);
                break;
            case(GetNorm_d):
                cout<<"Array matrix norm:\n"<<matrix_1->GetNorm()<<"\nList matrix norm:\n"<<matrix_2->GetNorm()<<endl;
                break;
            case(Print_d):
                printf("Array matrix\n");
                printarraymatrix_d(matrix_1);
                printf("List matrix\n");
                printlistmatrix_d(matrix_2);
                break;
            case(Back_d):
                break;
            default:
                printf("Repid input");
                break;
        }
    } while(tipe != 5);
}

template<typename T>
void printlistmatrix_t(ListTriangleMatrix<T> * matrix) {
    int size = matrix->GetDimension();
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (i <= j) {
                cout << matrix->Get(i, j) << " ";
            } else {
                cout << "0 ";
            }
        }
        cout<<endl;
    }
    cout<<endl;
}

template<typename T>
void printarraymatrix_t(ArrayTriangleMatrix<T> * matrix) {
    int size = matrix->GetDimension();
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (i <= j) {
                cout << matrix->Get(i, j) << " ";
            } else {
                cout << "0 ";
            }
        }
        cout<<endl;
    }
    cout<<endl;
}

void Triangle_matrix_menu() {
    Dmatrix tipe_1 = Back_d;
    int tipe = tipe_1;
    int n, place_from, place_in;


    printf("Input size of matrix\n");
    cin >> n;
    cout << "Input element of matrix, size :" << n << endl;
    int **a;
    a = new int *[n];
    for (int i = 0; i < n; i++) {
        a[i] = new int[n];
        for (int j = 0; j < n; j++) {
            if (i <= j) {
                cin >> a[i][j];
            } else {
                a[i][j] = 0;
            }
        }
    }

    ArrayTriangleMatrix<int> * matrix_1 = new ArrayTriangleMatrix<int>(n, a);
    ListTriangleMatrix<int> * matrix_2 = new ListTriangleMatrix<int>(n, a);

    for (int i = 0; i < n; i++) {
        delete a[i];
    }
    delete a;

    do {
        Dmatrixmenu();
        cin>>tipe;
        switch(tipe) {
            case(GetDimension_d):
                cout<<"Array matrix dimension:\n"<<matrix_1->GetDimension()<<"\nList matrix dimension:\n"<<matrix_2->GetDimension()<<endl;
                break;
            case(MultScalar_d):
                printf("Input coefficient:\n");
                cin>>n;
                matrix_1->MultScalar(n);
                matrix_2->MultScalar(n);
                break;
            case(GetNorm_d):
                cout<<"Array matrix norm:\n"<<matrix_1->GetNorm()<<"\nList matrix norm:\n"<<matrix_2->GetNorm()<<endl;
                break;
            case(Print_d):
                printf("Array matrix\n");
                printarraymatrix_t(matrix_1);
                printf("List matrix\n");
                printlistmatrix_t(matrix_2);
                break;
            case(Back_d):
                break;
            default:
                printf("Repid input");
                break;
        }
    } while(tipe != 5);
}


#endif //LAB3_MENUS_H